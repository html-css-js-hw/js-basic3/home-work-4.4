// ТЗ 1
let number1;
let number2;

while (isNaN(number1) || !Number.isInteger(number1)) {
    let input1 = prompt("Введіть перше ціле число:");
    number1 = parseInt(input1);
}
while (isNaN(number2) || !Number.isInteger(number2)) {
    let input2 = prompt("Введіть друге ціле число:");
    number2 = parseInt(input2);
}
let smallerNumber, largerNumber;
if (number1 <= number2) {
    smallerNumber = number1;
    largerNumber = number2;
} else {
    smallerNumber = number2;
    largerNumber = number1;
}
console.log(`Цілі числа від ${smallerNumber} до ${largerNumber}:`);
for (let i = smallerNumber; i <= largerNumber; i++) {
    console.log(i);
};

//ТЗ 2
let userInput;
let isEven = false;

while (!isEven) {
    userInput = prompt("Введіть парне число:");
    let number = parseInt(userInput);
    if (!isNaN(number) && number % 2 === 0) {
        isEven = true;
        console.log(`Ви ввели парне число: ${number}`);
    } else {
        console.log("Введене число не є парним. Будь ласка, введіть парне число.");
    }
}